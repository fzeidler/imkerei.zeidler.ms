---
title: "Imkerei Zeidler"
date: 2019-08-08T17:55:52+02:00
draft: false
---


Gremmendorfer Gold

Honig aus Gremmendorf

Direkt beim Imker kaufen: Christian Zeidler 48167 MS-Gremmendorf, Anton-Knubel-Weg 62 (täglich zwischen 9:00 und 18:00 Uhr, 0251 619302 kurze Voranmeldung erbeten)

Von Februar bis November sammeln meine Bienen von den Blüten und Blättern aus ihrer Nachbarschaft Nektar und Honigtau und machen daraus wohlschmeckenden Honig für Sie.

Honig ist ein reines und unverfälschtes Naturprodukt, das Sie nur vom Imker (oder von seinen Vertriebspartnern) in dieser garantierten Qualität erhalten können.

Kommen Sie am 28. oder 29.9.2019 auf dem Gremmendorfer Stadtteilfest in der ehemaligen Yorkkaserne in Gremmendorf mit mir an meinem Stand ins Gespräch.

Ich verkaufe Honig und spreche gern mit Ihnen über die Bienenhaltung und die guten Wirkungen des Honigs.

Natürlich können Sie bei mir auch ein Bienenvolk für Ihre eigene (neue) Imkerei oder etwas Wachs für Ihre Kosmetikprodukte kaufen.

Leere Honiggläser sind immer willkommen. 